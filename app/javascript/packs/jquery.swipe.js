// swipe events for touch devices

$.fn.swipeEvents = function(direction) {

    return this.each(function() {

        var $el = $(this),
            startX,
            startY,
            startEvent;

        $el.on({
            'touchstart': touchstart,
            'touchend':   touchend
        });

        function touchstart(event) {

            var touches = event.originalEvent.touches;

            if (touches && touches.length) {
                startEvent = event;
                startX = touches[0].pageX;
                startY = touches[0].pageY;
                $el.on('touchmove', touchmove);
            }

        }

        function touchmove(event) {

            var touches = event.originalEvent.touches;

            if (touches && touches.length) {

                var deltaX = startX - touches[0].pageX,
                    deltaY = startY - touches[0].pageY;

                if (direction == 'horizontal' && Math.abs(deltaX) >= 50 || direction == 'vertical' && Math.abs(deltaY) >= 50) {
                    event.preventDefault();
                }
                if (deltaX >= 50 && direction != 'vertical') {
                    $el.trigger('swipeLeft', startEvent);
                }
                if (deltaX <= -50 && direction != 'vertical') {
                    $el.trigger('swipeRight', startEvent);
                }
                if (deltaY >= 50 && direction != 'horizontal') {
                    $el.trigger('swipeUp', startEvent);
                }
                if (deltaY <= -50 && direction != 'horizontal') {
                    $el.trigger('swipeDown', startEvent);
                }
                if (Math.abs(deltaX) >= 50 || Math.abs(deltaY) >= 50) {
                    touchend();
                }

            }

        }

        function touchend() {

            $el.off('touchmove', touchmove);

        }

    });

};